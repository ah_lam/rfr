# Realtime File Replicator
Realtime File Replicator (RFR) is for replicating files from one host (source) to another host (destination). In source host, RFR monitors the file changes (create, update and delete) events, and pushes the delta to destination. 

## Use Cases

RFR is specialized for replicating continuously growthing files, e.g:
* log files
* QuickFix file store


## Configuration
The config file is in YAML format and filename should be `application.yml`. You can refer to `src/test/resource/application-localtest.yml` as an example.

| Config item | Description |
| ------------- |:-------------| 
| destination | the root path of the replicated file should write to. |
| port | the TCP port opened on source host |
| host | the hostname of source host |
| sourceList | contains a list of sources. Each source have a name, dir, and filename patterns. |
| sourceList:\<src name\>:dir |  the source directory for RFR to monitor. |
| sourceList:\<src name\>:patterns: | contains a list of filename patterns RFR should monitor. (the default file pattern is `*.*`) |

## Build
RFR using Maven to build. So, you can use stand maven package command:
```
./mvnw package
```
After build succeed, it should find the jar file at `target/rfr-*.jar`

## Usage

**Important** : Assuming you have the `application.yml` config file ready and place it in current working directory. 

First you need to run RFR at source host:
```
java -jar rfr-xxxx.jar source
```
Then, you need to run RFR at destination host:
```
java -jar rfr-xxxx.jar dest
```

## TODO
1. Data Compression
2. Detect file rename
3. Support SSL

## Limitation 
Due to performance consideraton, in file update event, the follows the following rules:
1. if file size is **not** larger than 512 bytes, RFR resends the whole file to destination host.
2. if the current file size is smaller than last read file size, RFR resend the whole file to destionation host.
3. if the current file size is same as current file size, do nothing. This means RFR **ignores** updating existing contents in the file.
4. if the current file size is larger than last read, RFR send the tail content of the file since last read to destination host.

(**Note** rules in lower number are higher in priority.)


