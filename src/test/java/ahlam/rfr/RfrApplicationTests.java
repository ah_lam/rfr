package ahlam.rfr;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.AccessMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.Random;

import static java.nio.file.StandardOpenOption.*;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "localtest")
@SpringBootTest
public class RfrApplicationTests {

	final static Logger log = LoggerFactory.getLogger(RfrApplication.class);

	final static Path TEST_ROOT_DIR = Paths.get("localtest");
	final static Path DEST_DIR = Paths.get("localtest/dest");
	final static Path DEST1_DIR = Paths.get("localtest/dest/src1");
	final static Path DEST2_DIR = Paths.get("localtest/dest/src2");
	final static Path SRC1_DIR = Paths.get("localtest/src1");
	final static Path SRC2_DIR = Paths.get("localtest/src2");
	final static Random random = new Random();


	@Autowired
	Config config;

	@Autowired
	ApplicationContext ctx;

	static boolean setupDone = false;

	SourceServer server;
	DestClient client;

	@Before
	public void setup() throws IOException, InterruptedException {
	    if (!setupDone) {
			deleteTree(TEST_ROOT_DIR);
			Files.createDirectories(SRC1_DIR);
			Files.createDirectories(SRC2_DIR);
			setupDone = true;
			Thread.sleep(500);
		}
		server = ctx.getBean(SourceServer.class);
		client = ctx.getBean(DestClient.class);
		Assert.assertNotNull(server);
		Assert.assertNotNull(client);
	}

	@Test
	public void testSyncASmallFile() throws IOException, InterruptedException, NoSuchAlgorithmException {
		String filename = "small.log";
		appendDataToFile(SRC1_DIR, filename, 30);
		assertFileEquals(SRC1_DIR, DEST1_DIR, filename);
		rewriteDataToFile(SRC1_DIR, filename, 29);
		assertFileEquals(SRC1_DIR, DEST1_DIR, filename);
	}

	@Test
	public void testSyncALargeFile() throws IOException, InterruptedException, NoSuchAlgorithmException {
		String filename = "large.log";
		appendDataToFile(SRC1_DIR, filename, 30);
		assertFileEquals(SRC1_DIR, DEST1_DIR, filename);
		appendDataToFile(SRC1_DIR, filename, 79);
		assertFileEquals(SRC1_DIR, DEST1_DIR, filename);
		appendDataToFile(SRC1_DIR, filename, 1000);
		assertFileEquals(SRC1_DIR, DEST1_DIR, filename);
		appendDataToFile(SRC1_DIR, filename, 1000);
		appendDataToFile(SRC1_DIR, filename, 1000);
		appendDataToFile(SRC1_DIR, filename, 1000);
		appendDataToFile(SRC1_DIR, filename, 1000);
		appendDataToFile(SRC1_DIR, filename, 1000);
		appendDataToFile(SRC1_DIR, filename, 1000);
		assertFileEquals(SRC1_DIR, DEST1_DIR, filename);
	}

	@Test
	public void testPatternMatching() throws IOException, NoSuchAlgorithmException, InterruptedException {
		String filenameLog = "match.log";
		String filenameTxt = "match.txt";
		String filenameTxtX = "match.txt.1";
		appendDataToFile(SRC1_DIR, filenameLog, 30);
		appendDataToFile(SRC1_DIR, filenameTxt, 30);
		appendDataToFile(SRC1_DIR, filenameTxtX, 30);
		appendDataToFile(SRC2_DIR, filenameLog, 30);
		appendDataToFile(SRC2_DIR, filenameTxt, 30);
		appendDataToFile(SRC2_DIR, filenameTxtX, 30);
		appendDataToFile(SRC1_DIR, filenameLog, 11030);
		appendDataToFile(SRC1_DIR, filenameTxt, 11030);
		appendDataToFile(SRC1_DIR, filenameTxtX, 11030);
		appendDataToFile(SRC2_DIR, filenameLog, 11030);
		appendDataToFile(SRC2_DIR, filenameTxt, 11030);
		appendDataToFile(SRC2_DIR, filenameTxtX, 11030);
		assertFileEquals(SRC1_DIR, DEST1_DIR, filenameLog);
		assertFileEquals(SRC2_DIR, DEST2_DIR, filenameTxt);
		assertFileEquals(SRC2_DIR, DEST2_DIR, filenameTxtX);
		assertFileNotExist(DEST1_DIR, filenameTxt);
		assertFileNotExist(DEST1_DIR, filenameTxtX);
		assertFileNotExist(DEST2_DIR, filenameLog);
	}

	@Test
	public void testDeleteFile() throws IOException, NoSuchAlgorithmException, InterruptedException {
		String filename = "delete.log";
		appendDataToFile(SRC1_DIR, filename, 30);
		assertFileEquals(SRC1_DIR, DEST1_DIR, filename);
		appendDataToFile(SRC1_DIR, filename, 29000);
		assertFileEquals(SRC1_DIR, DEST1_DIR, filename);
		deleteFile(SRC1_DIR, filename);
		assertFileNotExist(DEST1_DIR, filename);
	}

	@Test
	public void testReconnect() throws IOException, NoSuchAlgorithmException, InterruptedException {
		String filename = "reconnect.log";
		appendDataToFile(SRC1_DIR, filename, 30);
		assertFileEquals(SRC1_DIR, DEST1_DIR, filename);
		appendDataToFile(SRC1_DIR, filename, 29000);
		assertFileEquals(SRC1_DIR, DEST1_DIR, filename);

		client.handleException(new Exception("force reconnect"));
		client.setReconnectInterval(1000);
		Thread.sleep(2000);

		appendDataToFile(SRC1_DIR, filename, 9000);
		assertFileEquals(SRC1_DIR, DEST1_DIR, filename);

	}


	private void assertFileNotExist(Path path, String filename) throws InterruptedException {
		int maxRetry = 100;
		int retryCnt=0;
		Path file = Paths.get(path.toString(), filename);
		while(Files.exists(file) && retryCnt < maxRetry) {
			Thread.sleep(10);
			retryCnt++;
		}
		Assert.assertFalse(Files.exists(file));
	}

	private void deleteFile(Path path, String filename) throws IOException {
		Path file = Paths.get(path.toString(), filename);
		Files.deleteIfExists(file);
	}

	private void assertFileEquals(Path srcDir, Path destDir, String filename) throws IOException, InterruptedException, NoSuchAlgorithmException {
	    int maxRetry = 100;
	 	Path srcFile = Paths.get(srcDir.toString(), filename);
		Path destFile = Paths.get(destDir.toString(), filename);
		long srcFileSize = Files.size(srcFile);
		int retryCnt=0;
		long destFileSize=-1;
		while (retryCnt < 100) {
			retryCnt++;
			Thread.sleep(10);
			if (!Files.exists(destFile))
				continue;
			destFileSize = Files.size(destFile);
			if (destFileSize < srcFileSize)
				continue;
			break;
		}
		Assert.assertEquals(srcFileSize, destFileSize);
		Assert.assertTrue(MessageDigest.isEqual(getMD5(srcFile), getMD5(destFile)));
	}

	private byte[] getMD5(Path file) throws IOException, NoSuchAlgorithmException {
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		SeekableByteChannel channel = Files.newByteChannel(file, READ);
		ByteBuffer bb  = ByteBuffer.allocate(4096);
		while(channel.read(bb) > 0) {
			bb.flip();
			md5.update(bb);
			bb.clear();
		}
		return md5.digest();
	}

	private void rewriteDataToFile(Path path, String fileName, int datasize) throws IOException {
		Path outfile = Paths.get(path.toString(), fileName);
		OutputStream outputStream = Files.newOutputStream(outfile, CREATE, TRUNCATE_EXISTING);
		writeRandomData(outputStream, datasize);
		outputStream.flush();
		outputStream.close();
	}

	private void appendDataToFile(Path path, String fileName, int datasize) throws IOException {
		Path outfile = Paths.get(path.toString(), fileName);
		OutputStream outputStream = Files.newOutputStream(outfile, CREATE, APPEND);
		writeRandomData(outputStream, datasize);
		outputStream.flush();
		outputStream.close();
	}

	private void writeRandomData(OutputStream os, int datasize) throws IOException {
		byte[] data = new byte[datasize];
        random.nextBytes(data);
        os.write(data);
	}

	private void deleteTree(Path path) throws IOException {
	    log.debug("deleting "+ path);
		if (!Files.exists(path))
			return;
		if (Files.isDirectory(path))
			Files.walk(path).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
		else
			Files.delete(path);
	}

}
