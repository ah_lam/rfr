package ahlam.rfr;

import ahlam.rfr.msg.DestFileStatus;
import ahlam.rfr.msg.Message;
import ahlam.rfr.msg.SourceDataBlock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.*;

public class DestWriter implements Runnable {

    final static Logger log = LoggerFactory.getLogger(SourceWatcher.class);


    static class FileStatus {
        MessageDigest md;
        long bytesWritten;
        Path file;
        SeekableByteChannel channel;
        String filename;

        byte[] getMD5() {
            try {
                return ((MessageDigest)md.clone()).digest();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public String toString() {
            return "FileStatus{" +
                    "md=" + md +
                    ", bytesWritten=" + bytesWritten +
                    ", file=" + file +
                    ", channel=" + channel +
                    ", filename='" + filename + '\'' +
                    '}';
        }
    }


    private final String srcName;
    private final String destDir;
    private BlockingQueue<Message> outBoundMsgQueue;
    private IExceptionHandler exceptionHandler;
    private BlockingQueue<SourceDataBlock> inBoundMsgQueue;
    private Path destPath;
    private List<PathMatcher> matchers;

    private Map<String, FileStatus> statusMap = new HashMap<>();

    private Thread thread;
    private boolean stop=false;


    public DestWriter(String srcName,
                      List<String> patterns,
                      String destDir,
                      BlockingQueue<Message> outBoundMsgQueue,
                      IExceptionHandler exceptionHandler) throws IOException {

        this.srcName = srcName;
        this.destDir = destDir;
        this.outBoundMsgQueue = outBoundMsgQueue;
        this.exceptionHandler = exceptionHandler;
        this.inBoundMsgQueue = new ArrayBlockingQueue<SourceDataBlock>(10000);
        destPath = Paths.get(destDir, srcName);
        matchers = patterns.stream().map(p-> FileSystems.getDefault().getPathMatcher("glob:" + p)).collect(Collectors.toList());
        if (!Files.isDirectory(destPath)) {
            Files.createDirectories(destPath);
        }
    }

    public void init() throws IOException, NoSuchAlgorithmException, InterruptedException {
        //scan the dir for existing file
        log.debug("scanning dir "+destPath);
        Stream<Path> walk = Files.walk(destPath, 1);
        List<Path> files= walk.filter(Files::isRegularFile)
                              .filter(Files::isReadable)
                              .filter( (p) -> matchers.stream().anyMatch(m->m.matches(p.getFileName())))
                              .collect(Collectors.toList());
        for(Path p: files) {
            String filename = p.getFileName().toString();
            if (statusMap.containsKey(filename)) {
                continue;
            }
            initFileStatusAndSend(filename, p);
        }
    }

    private void initFileStatusAndSend(String filename, Path p) throws NoSuchAlgorithmException, IOException, InterruptedException {
        //check message digest
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        SeekableByteChannel channel = Files.newByteChannel(p, READ);
        ByteBuffer bb = ByteBuffer.allocate(4096);
        long totalRead = 0;
        long read = 0;
        while( (read = channel.read(bb)) > 0) {
            bb.flip();
            md5.update(bb);
            totalRead += read;
            bb.clear();
        }
        channel.close();

        //populate FileStatus
        FileStatus fs = new FileStatus();
        fs.bytesWritten = totalRead;
        fs.channel = Files.newByteChannel(p, WRITE, APPEND);
        fs.channel.position(totalRead);
        fs.file = p;
        fs.filename = filename;
        fs.md = md5;
        statusMap.put(filename, fs);

        log.info("detected existing file with status " + fs);

        //send the message
        DestFileStatus dfs = new DestFileStatus();
        dfs.setSrcName(srcName);
        dfs.setFilename(filename);
        dfs.setChecksum(fs.getMD5());
        dfs.setFilesize(fs.bytesWritten);

        outBoundMsgQueue.put(dfs);
    }

    public void start() {
        if (thread==null) {
            stop=false;
            thread = new Thread(this);
            thread.start();
        }

    }

    @Override
    public void run() {
        while(!stop) {
            try {
                Message msg = inBoundMsgQueue.poll(100, TimeUnit.MILLISECONDS);
                if (msg==null)
                    continue;

                if (msg instanceof SourceDataBlock) {
                    handleSourceDataBlock((SourceDataBlock) msg);
                }
                else {
                    log.warn("Invalid message type: " + msg.getClass());
                }

            } catch (InterruptedException e) {
                return;
            } catch (Exception e) {
                if (!exceptionHandler.handleException(e)) {
                    return;
                }
            }
        }
    }

    public void enqueSourceDataBlock(SourceDataBlock msg) throws InterruptedException {
        inBoundMsgQueue.put(msg);
    }

    private void handleSourceDataBlock(SourceDataBlock msg) throws IOException, NoSuchAlgorithmException {
        FileStatus fs = statusMap.get(msg.getFilename());
        if (fs==null) {
            if (msg.getOffset() < 0) {
                return;
            }
            //init FileStatus
            fs = new FileStatus();
            fs.filename = msg.getFilename();
            fs.file = Paths.get(destPath.toString(), fs.filename);
            fs.channel = Files.newByteChannel(fs.file, WRITE, TRUNCATE_EXISTING, CREATE);
            fs.md = MessageDigest.getInstance("MD5");
            statusMap.put(msg.getFilename(), fs);
        }
        if (msg.getOffset() < 0) {
            //delete case
            try {
                Files.delete(fs.file);
            } catch (FileNotFoundException e) {
                log.warn("File {} already removed", fs.filename);
            }

            fs.channel.close();
            statusMap.remove(fs.filename);
            return;
        }
        else if (msg.getOffset() == 0) {
            //rewrite the file
            fs.bytesWritten = 0;
            fs.channel.position(0);
            fs.channel.truncate(0);
            fs.md.reset();
        }

        if (fs.bytesWritten == msg.getOffset()) {
            //normal append case
            fs.md.update(msg.getData());
            ByteBuffer bb = ByteBuffer.wrap(msg.getData());
            int written = 0;
            while (written < msg.getData().length) {
                int wrote = fs.channel.write(bb);
                written += wrote;
            }
            fs.bytesWritten += msg.getData().length;
            if (!MessageDigest.isEqual(msg.getChecksum(), fs.getMD5())) {
                //error, corrupted file.
                String errMsg = "Message Digest Mis-match for file " + fs.file;
                log.error(errMsg);
                throw new IOException(errMsg);
            }
        }
        else {
            String errMsg ="Source data doesn't match destination for file "+fs.file+". (expected offset = " + msg.getOffset() + ", destination file size = " + fs.bytesWritten + ").";
            log.error(errMsg);
            throw new IOException(errMsg);
        }

    }

    public void stop() {
        stop = true;
        if (thread!=null) {
            try {
                thread.join(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (thread.isAlive()) {
                thread.interrupt();
            }
            thread = null;
        }
        cleanup();
    }

    private void cleanup() {
        inBoundMsgQueue.clear();
        while (!statusMap.isEmpty()) {
            try {
                statusMap.forEach((k, v) -> {
                    try {
                        v.channel.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                statusMap.clear();
            } catch (ConcurrentModificationException e) {
                log.warn("{}", e);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    log.warn("{}", ex);
                }
            }
        }
        log.info("cleanup done {}, {}", srcName, destDir);
    }


}
