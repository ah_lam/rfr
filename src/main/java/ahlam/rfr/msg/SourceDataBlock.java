package ahlam.rfr.msg;

import java.io.Serializable;
import java.util.Arrays;

public class SourceDataBlock extends Message implements Serializable{


	protected String srcName;
	protected String filename;
	protected long offset;
	protected byte[] data;
	protected byte[] checksum;

	public String getSrcName() {
		return srcName;
	}

	public void setSrcName(String srcName) {
		this.srcName = srcName;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public long getOffset() {
		return offset;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public byte[] getChecksum() {
		return checksum;
	}

	public void setChecksum(byte[] checksum) {
		this.checksum = checksum;
	}

	@Override
	public String toString() {
		return "SourceDataBlock{" +
				"srcName='" + srcName + '\'' +
				", filename='" + filename + '\'' +
				", offset=" + offset +
				", checksum=" + Arrays.toString(checksum) +
				", data=" + Arrays.toString(data) +
				'}';
	}
}
