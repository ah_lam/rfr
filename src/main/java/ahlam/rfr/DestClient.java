package ahlam.rfr;

import ahlam.rfr.msg.Message;
import ahlam.rfr.msg.ReadyToReceiveSourceDataBlock;
import ahlam.rfr.msg.SourceDataBlock;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
@Lazy
public class DestClient extends IoHandlerAdapter implements IExceptionHandler{
    static final Logger log = LoggerFactory.getLogger(DestClient.class);

    private final Config config;
    private final IoConnector connector;

    private MessageSender messageSender;
    private BlockingQueue<Message> outBoundMsgQueue;
    private DestWriterSet writerSet;
    private boolean stop = false;
    private IoSession session;

    @Value("${rfr.reconnectInterval:5000}")
    private int reconnectInterval = 5000;

    public DestClient(Config config, IoConnector connector) throws IOException {
        this.config = config;
        this.connector = connector;
        this.connector.setHandler(this);
        outBoundMsgQueue = new ArrayBlockingQueue<>(10000);
        writerSet = new DestWriterSet(config, outBoundMsgQueue, this);
    }

    @PostConstruct
    public void init() {
        connect();
    }

    @Override
    public void sessionOpened(IoSession session) throws Exception {
        messageSender = new MessageSender(outBoundMsgQueue, session);
        messageSender.start();
        writerSet.init();
        writerSet.start();
        this.session = session;
        outBoundMsgQueue.put(new ReadyToReceiveSourceDataBlock());
    }

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        if (message instanceof SourceDataBlock) {
            writerSet.enqueMsg((SourceDataBlock) message);
        }
        else {
            log.warn("Unknown message type: " + message.getClass());
        }
    }

    @Override
    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        log.warn("Something wrong in communication channel. Closing the connection.", cause);
        session.closeNow();
    }

    @Override
    public boolean handleException(Exception e) {
        log.warn("Something wrong in when writing files. Closing the connection.", e);
        if (session!=null)
            session.closeNow();
        return false;
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {
        log.warn("Connection closed");
        reconnect();
        this.session = null;
    }

    private void connect() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    while (!stop) {
                        log.info("connecting....");
                        ConnectFuture future = connector.connect(new InetSocketAddress(config.host, config.port));
                        future.await();
                        if (future.getException() != null) {
                            log.error("connection fail", future.getException());
                        }
                        if (future.isConnected()) {
                            log.info("connected");
                            return;
                        }
                        Thread.sleep(reconnectInterval);
                    }
                } catch (Exception e) {
                    log.error("error during connecting", e);
                }
                finally {
                    executorService.shutdown();
                }
            }
        });

    }


    private void reconnect() throws InterruptedException {
        cleanup();
        Thread.sleep(reconnectInterval);
        if (!stop) {
            connect();
        }
    }

    private void cleanup() {
        if (writerSet!=null)
            writerSet.stop();
        if (outBoundMsgQueue != null)
            outBoundMsgQueue.clear();
        if (messageSender != null)
            messageSender.stop();
    }

    @PreDestroy
    public void stop() {
        stop=true;
        session.closeNow();
    }

    public int getReconnectInterval() {
        return reconnectInterval;
    }

    public void setReconnectInterval(int reconnectInterval) {
        this.reconnectInterval = reconnectInterval;
    }
}
