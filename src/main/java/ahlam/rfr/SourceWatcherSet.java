package ahlam.rfr;


import ahlam.rfr.msg.DestFileStatus;
import ahlam.rfr.msg.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

public class SourceWatcherSet {
    static Logger log = LoggerFactory.getLogger(SourceWatcherSet.class);

    Map<String, SourceWatcher> watcherMap = new HashMap<>();
    private BlockingQueue<Message> queue;


    public SourceWatcherSet(Config config, BlockingQueue<Message> queue) throws IOException {
        this.queue = queue;

        try {
            for (Map.Entry<String, Config.Source> e : config.getSourceList().entrySet()) {
                watcherMap.put(e.getKey(), new SourceWatcher(e.getValue().getDir(), e.getValue().getPatterns(), e.getKey(), queue));
            }
        }
        catch (Exception e) {
            stop();
            throw e;
        }
    }

    public void start() {
        for (SourceWatcher w : watcherMap.values()) {
            w.start();
        }
    }

    public void processDestFileStatus(DestFileStatus dfs) throws NoSuchAlgorithmException, CloneNotSupportedException, IOException {
        SourceWatcher sourceWatcher = watcherMap.get(dfs.getSrcName());
        if (sourceWatcher!=null) {
            sourceWatcher.processDestFileStatus(dfs);
        }
        else {
            log.warn("Fail to get SourceWather for source" + dfs.getSrcName());
        }
    }

    public void stop() {
        for (SourceWatcher w : watcherMap.values()) {
            w.stop();
        }
    }

    public BlockingQueue<Message> getQueue() {
        return queue;
    }

    public void setQueue(BlockingQueue<Message> queue) {
        this.queue = queue;
    }


}
