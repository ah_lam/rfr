package ahlam.rfr;

import ahlam.rfr.msg.DestFileStatus;
import ahlam.rfr.msg.Message;
import ahlam.rfr.msg.ReadyToReceiveSourceDataBlock;
import ahlam.rfr.msg.SourceDataBlock;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Lazy
public class SourceServer extends IoHandlerAdapter{

    static Logger log = LoggerFactory.getLogger(SourceServer.class);

    IoAcceptor acceptor;
    Config config;

    Map<IoSession, SourceWatcherSet> watcherSetMap = new ConcurrentHashMap<>();
    Map<IoSession, MessageSender> senderMap = new ConcurrentHashMap<>();

    public SourceServer(Config config, IoAcceptor acceptor) {
        this.config = config;
        this.acceptor = acceptor;
        acceptor.setHandler(this);
    }

    @PostConstruct
    void init() throws IOException {
        acceptor.bind(new InetSocketAddress(config.getPort()));
    }

    @Override
    public void sessionOpened(IoSession session) throws Exception {
        ArrayBlockingQueue<Message> queue = new ArrayBlockingQueue<>(1000);
        SourceWatcherSet sws = new SourceWatcherSet(config, queue);
        MessageSender messageSender = new MessageSender(queue, session);
        messageSender.start();
        watcherSetMap.put(session, sws);
        senderMap.put(session, messageSender);
    }

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        SourceWatcherSet sws = watcherSetMap.get(session);
        log.debug("Recevied message " + message.getClass());
        if (sws!=null) {
            if ( message instanceof DestFileStatus) {
                sws.processDestFileStatus((DestFileStatus) message);
            }
            else if (message instanceof ReadyToReceiveSourceDataBlock) {
                log.debug("received ReadyToReceiveSourceDataBlaock");
                sws.start();
            }
        }
    }

    @Override
    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        log.warn("Something wrong in communication channel, closing it.", cause);
        cleanupSession(session);
        session.closeNow();
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {
        cleanupSession(session);
    }

    private void cleanupSession(IoSession session) {
        SourceWatcherSet sws = watcherSetMap.remove(session);
        if (sws!=null) {
            sws.stop();
        }
        MessageSender messageSender = senderMap.remove(session);
        if (messageSender!=null) {
            messageSender.stop();
        }
    }

    @PreDestroy
    void destroy() {
        acceptor.unbind();
        watcherSetMap.forEach((k,v) -> v.stop());
        watcherSetMap.clear();
    }


}
