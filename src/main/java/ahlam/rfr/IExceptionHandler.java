package ahlam.rfr;

public interface IExceptionHandler {

    /**
     *
     * @param e
     * @return false = fatal exception; true = can ignore the exception;
     */
    boolean handleException(Exception e);

}
