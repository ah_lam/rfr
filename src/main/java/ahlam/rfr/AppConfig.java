package ahlam.rfr;

import ahlam.rfr.msg.Message;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.serialization.ObjectSerializationCodecFactory;
import org.apache.mina.filter.logging.LogLevel;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.apache.mina.transport.socket.nio.NioSocketConnector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@Configuration
public class AppConfig {

    @Bean @Lazy
    public IoAcceptor acceptor(LoggingFilter loggingFilter) {
        NioSocketAcceptor acceptor = new NioSocketAcceptor();
        ProtocolCodecFilter codecFilter = new ProtocolCodecFilter(new ObjectSerializationCodecFactory());
        acceptor.getFilterChain().addLast("lg", loggingFilter);
        acceptor.getFilterChain().addLast("codec", codecFilter);
        acceptor.getSessionConfig().setKeepAlive(true);
        acceptor.getSessionConfig().setReuseAddress(true);
        acceptor.getSessionConfig().setReadBufferSize(1024*100);
        acceptor.setReuseAddress(true);
        return acceptor;
    }

    @Bean @Lazy
    public IoConnector connector(Config config, LoggingFilter loggingFilter) {
        NioSocketConnector connector = new NioSocketConnector();
        ProtocolCodecFilter codecFilter = new ProtocolCodecFilter(new ObjectSerializationCodecFactory());
        connector.getFilterChain().addLast("codec",codecFilter);
        connector.getFilterChain().addLast("lg", loggingFilter);
        connector.setConnectTimeoutCheckInterval(20000);
        return connector;
    }

    @Bean @Lazy
    public LoggingFilter loggingFilter() {
        LoggingFilter ret = new LoggingFilter();
        ret.setMessageReceivedLogLevel(LogLevel.DEBUG);
        ret.setMessageSentLogLevel(LogLevel.DEBUG);
        return ret;
    }
}
