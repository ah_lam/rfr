package ahlam.rfr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class RfrApplication {

	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Invalid number of argument");
			return;
		}
		ConfigurableApplicationContext ctx = SpringApplication.run(RfrApplication.class, args);
		switch(args[0]) {
			case "source":
				SourceServer sourceServer = ctx.getBean(SourceServer.class);
				break;
			case "dest":
				DestClient destClient = ctx.getBean(DestClient.class);
				break;
			default:
				System.out.println("Invalid argument: '"+args[0]+"'");
				break;

		}
	}


}
