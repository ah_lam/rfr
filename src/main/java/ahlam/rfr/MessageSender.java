package ahlam.rfr;

import ahlam.rfr.msg.Message;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class MessageSender implements Runnable{

    final static Logger log = LoggerFactory.getLogger(MessageSender.class);

    private final BlockingQueue<Message> queue;
    private final IoSession session;
    boolean stop = false;
    Thread thread;

    public MessageSender(BlockingQueue<Message> queue, IoSession session) {

        this.queue = queue;
        this.session = session;
    }



    @Override
    public void run() {
        while (!stop) {
            try {
                Message msg = queue.poll(100, TimeUnit.MILLISECONDS);
                if (msg!=null) {
                    log.debug("sending mesg " + msg.getClass());
                    WriteFuture future = session.write(msg);
                    future.await();
                }
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    synchronized public void start() {
        if (thread==null) {
            stop=false;
            thread = new Thread(this);
            thread.start();
        }
    }

    synchronized public void stop() {
        queue.clear();
        stop = true;
        if (thread!=null) {
            try {
                thread.join(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (thread.isAlive()) {
                thread.interrupt();
            }
            thread = null;
        }
    }
}
