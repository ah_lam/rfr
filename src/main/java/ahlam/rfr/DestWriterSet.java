package ahlam.rfr;

import ahlam.rfr.msg.Message;
import ahlam.rfr.msg.SourceDataBlock;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;

public class DestWriterSet {

    private Map<String, DestWriter> writerMap = new HashMap<>();
    IExceptionHandler exceptionHandler;


    public DestWriterSet(Config config, BlockingQueue<Message> outBoundMsgQueue, IExceptionHandler exceptionHandler) throws IOException {
        this.exceptionHandler = exceptionHandler;
        for(Map.Entry<String, Config.Source> e : config.getSourceList().entrySet() ) {
            DestWriter dw = new DestWriter(e.getKey(), e.getValue().getPatterns(), config.destination, outBoundMsgQueue, exceptionHandler);
            writerMap.put(e.getKey(), dw);
        }
    }

    public void init() throws InterruptedException, NoSuchAlgorithmException, IOException {
        for (Map.Entry<String, DestWriter> e:  writerMap.entrySet()) {
            e.getValue().init();
        }
    }

    public void start() {
        for (Map.Entry<String, DestWriter> e:  writerMap.entrySet()) {
            e.getValue().start();
        }
    }

    public void enqueMsg(SourceDataBlock msg ) throws InterruptedException {
        DestWriter destWriter = writerMap.get(msg.getSrcName());
        if (destWriter==null) {
            //TODO
            return;
        }
        destWriter.enqueSourceDataBlock(msg);
    }

    public void stop() {
        for (Map.Entry<String, DestWriter> e:  writerMap.entrySet()) {
            e.getValue().stop();
        }
    }
}
