package ahlam.rfr;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Configuration
@ConfigurationProperties(prefix="rfr")
public class Config {

    static class Source {
        protected String dir;
        protected List<String> patterns = new ArrayList<>();

        public Source() {
            patterns.add("*.*"); //default value
        }

        public String getDir() {
            return dir;
        }

        public void setDir(String dir) {
            this.dir = dir;
        }

        public List<String> getPatterns() {
            return patterns;
        }

        public void setPatterns(List<String> patterns) {
            this.patterns = patterns;
        }
    }

    protected String destination;
    protected Map<String, Source> sourceList = new HashMap<>();
    protected int port;
    protected String host;

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Map<String, Source> getSourceList() {
        return sourceList;
    }

    public void setSourceList(Map<String, Source> sourceList) {
        this.sourceList = sourceList;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public String toString() {
        return "Config{" +
                "destination='" + destination + '\'' +
                ", sourceList=" + sourceList +
                ", port=" + port +
                ", host='" + host + '\'' +
                '}';
    }
}
