package ahlam.rfr;

import ahlam.rfr.msg.DestFileStatus;
import ahlam.rfr.msg.Message;
import ahlam.rfr.msg.SourceDataBlock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardWatchEventKinds.*;

public class SourceWatcher implements Runnable {

    static class FileStatus {
        MessageDigest md;
        long bytesRead;
        Path file;
        SeekableByteChannel channel;
        String filename;

        public FileStatus(String filename, Path p) throws NoSuchAlgorithmException, IOException {
            this.md = MessageDigest.getInstance("MD5");
            this.bytesRead = 0;
            this.file = p;
            this.channel = Files.newByteChannel(p, READ);
            this.filename = filename;
        }

        public FileStatus() {

        }

        synchronized void reset() throws IOException {
            bytesRead =0;
            if (channel!=null) {
                channel.position(0);
            }
            else {
                channel=Files.newByteChannel(file, READ);
            }
            md.reset();
        }

        synchronized void close() throws IOException {
            bytesRead = 0;
            md.reset();
            if (channel!=null) {
                channel.close();
                channel=null;
            }
        }

        byte[] getMD5() {
            try {
                return ((MessageDigest)md.clone()).digest();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            return null;
        }

    }

    static class EventTask implements Runnable{

        private final FileStatus fs;
        private final WatchEvent.Kind<Path> event;
        private final SourceWatcher sw;

        public EventTask(FileStatus fs, WatchEvent.Kind<Path> event, SourceWatcher sw) {

            this.fs = fs;
            this.event = event;
            this.sw = sw;
        }

        @Override
        public void run() {
            if (event.equals(ENTRY_DELETE)) {
                SourceDataBlock msg = new SourceDataBlock();
                msg.setFilename(fs.filename);
                msg.setOffset(-1);
                sw.enqueMsg(msg);
                sw.statusMap.remove(fs.filename);
                try {
                    fs.close();
                }
                catch (IOException e) {
                    //do nothing
                }
            }
            else {
                try {
                    synchronized (fs) {
                        long size = Files.size(fs.file);
                        if (size == fs.bytesRead && size > 512) {
                            //file not changed
                            return;
                        }

                        byte[] lastChecksum = null;
                        if (size < fs.bytesRead || size < 512) {
                            if (size == fs.bytesRead) {
                                lastChecksum = fs.getMD5();
                            }
                            //the file seems rewritten. Re-read the file from starting
                            fs.reset();
                        }
                        ByteBuffer bb = ByteBuffer.allocateDirect(4096);
                        for(;;) {
                            bb.clear();
                            int read = fs.channel.read(bb);
                            if (read<=0)
                                break;
                            byte[] data = new byte[read];
                            bb.flip();
                            bb.get(data);
                            SourceDataBlock msg = new SourceDataBlock();
                            msg.setFilename(fs.filename);
                            msg.setOffset(fs.bytesRead);
                            msg.setData(data);
                            fs.md.update(data);
                            msg.setChecksum(fs.getMD5());
                            fs.bytesRead +=read;
                            if (lastChecksum == null || !MessageDigest.isEqual(msg.getChecksum(), lastChecksum)) {
                                sw.enqueMsg(msg);
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void enqueMsg(SourceDataBlock msg) {
        msg.setSrcName(srcName);
        log.debug("enqueMsg:" + msg);
        try {
            while (!queue.offer(msg, 100, TimeUnit.MILLISECONDS) && !stop) {

            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    final static Logger log = LoggerFactory.getLogger(SourceWatcher.class);

    Map<String, FileStatus> statusMap = new HashMap<>();
    Path dir;
    String srcName;
    WatchService watchService;
    List<PathMatcher> matchers;

    ExecutorService exService;
    BlockingQueue<Message> queue;
    Thread thread;
    boolean stop = false;

    public SourceWatcher(String path, List<String> patterns, String srcName, BlockingQueue<Message> queue) throws IOException {
        this.srcName = srcName;
        this.queue = queue;
        dir = FileSystems.getDefault().getPath(path);
        if (!dir.toFile().isDirectory()) {
            throw new IllegalArgumentException("Not a directory : " + path);
        }

        watchService = FileSystems.getDefault().newWatchService();

        matchers = patterns.stream().map(p->FileSystems.getDefault().getPathMatcher("glob:"+p)).collect(Collectors.toList());

        log.info("watching " + dir + " for pattern: " + String.join(",",patterns));
    }

    @Override
    public void run() {
        try {
            try {
                scanDir();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return;
            }


            statusMap.forEach((e,v) -> {
                exService.submit(new EventTask(v, ENTRY_MODIFY, this));
            });

            try {
                dir.register(watchService, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
            } catch (IOException e) {
                e.printStackTrace();
            }

            log.debug("start monitoring dir {}", dir);

            while (!stop) {

                WatchKey key;
                try {
                    key = watchService.poll(1000, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    return;
                }
                if (key!=null) {
                    try {
                        List<WatchEvent<?>> watchEvents = key.pollEvents();
                        log.debug("poolEvents:");
                        for (WatchEvent e : watchEvents) {
                            log.debug(">> Event: {}, file: {}", e.kind(), e.context());
                            if (!matchers.stream().anyMatch(m -> m.matches(Paths.get(e.context().toString())))) {
                                log.debug("file pattern not match, skip file {}", e.context());
                                continue;
                            }
                            if (e.kind().equals(OVERFLOW)) {
                                log.warn("event overflow: {}", e.context());
                            }
                            FileStatus fs = statusMap.get(e.context().toString());
                            if (fs != null) {
                                exService.submit(new EventTask(fs, e.kind(), this));
                            } else {
                                if (!e.kind().equals(ENTRY_CREATE)) {
                                    log.warn("unable to find FileStatus for {}, handle it as file creation.", e.context());
                                }
                                if (e.kind().equals(ENTRY_CREATE) || e.kind().equals(ENTRY_MODIFY)) {
                                    try {
                                        fs = new FileStatus(e.context().toString(), Paths.get(dir.toString(), e.context().toString()));
                                        statusMap.put(e.context().toString(), fs);
                                        exService.submit(new EventTask(fs, e.kind(), this));
                                    } catch (NoSuchAlgorithmException e1) {
                                        e1.printStackTrace();
                                    } catch (IOException e1) {
                                        return;
                                    }
                                } else {
                                    log.warn("unable to find FileStatus for " + e.context());
                                }
                            }
                        }
                    }
                    finally {
                        key.reset();
                    }
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        }
        finally {
            cleanup();
        }

    }

    private void cleanup() {
        if (exService!=null) {
            exService.shutdown();
        }
        try {
            if (watchService!=null) {
                watchService.close();
                watchService = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        statusMap.forEach( (e,v) -> {
            try {
                v.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
        statusMap.clear();
    }

    private void scanDir() throws IOException, NoSuchAlgorithmException {
        log.debug("scanning dir "+dir);
        try (Stream<Path> walk = Files.walk(dir, 1)) {
            List<Path> files = walk.filter(Files::isRegularFile)
                    .filter(Files::isReadable)
                    .filter((p) -> matchers.stream().anyMatch(m -> m.matches(p.getFileName())))
                    .collect(Collectors.toList());
            for (Path p : files) {
                String filename = p.getFileName().toString();
                if (statusMap.containsKey(filename)) {
                    continue;
                }
                FileStatus fs = new FileStatus(filename, p);
                statusMap.put(filename, fs);
                log.info("find file " + p);
            }
        }
    }

    public void processDestFileStatus(DestFileStatus status) throws IOException, NoSuchAlgorithmException, CloneNotSupportedException {
        log.debug("got DestFileStatus " + status);
        final Path filename = Paths.get(status.getFilename());

        //check the file pattern
        if (!matchers.stream().anyMatch(m->m.matches(filename))) {
            log.warn("receive DestFileStatus of file "+status.getFilename()+" not matching the file pattern " + matchers.stream().map(m->m.toString()).collect(Collectors.joining(",")));
            return;
        }

        //check if file exists
        Path file = Paths.get(dir.toString(), status.getFilename());
        if (!Files.exists(file))  {
            log.warn("receive DestFileStatus of file "+file+" not exists. Ignore." );
            return;
        }

        if (Files.size(file) < status.getFilesize() ) {
            log.warn("receive DestFileStatus of file "+file+" is larger than source ("+status.getFilesize()+" vs "+Files.size(file) +"). Ignore." );
            return;
        }

        //compare the MD5 checksum
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        SeekableByteChannel channel = Files.newByteChannel(file, READ);
        ByteBuffer bb = ByteBuffer.allocate(4096);
        long totalRead = 0;
        while (totalRead < status.getFilesize()) {
            bb.clear();
            int read = channel.read(bb);
            if (read <=0)
                return;

            if (totalRead + read <= status.getFilesize()) {
                bb.flip();
                md5.update(bb);
            }
            else {
                md5.update(bb.array(),0,(int)(status.getFilesize() - totalRead));
            }

            totalRead += read;
        }

        byte[] srcDigest = ((MessageDigest) md5.clone()).digest();
        if (!MessageDigest.isEqual(srcDigest, status.getChecksum())) {
            log.warn("receive DestFileStatus of file "+file+" with wrong checksum compare the source file. Ignore.");
            log.debug("srcDigest = " + java.util.Arrays.toString(srcDigest));
            log.debug("status.getChecksum = " + java.util.Arrays.toString(status.getChecksum()));
            return;
        }

        //checking pass, create the FileStatus Entry
        FileStatus fs = new FileStatus();
        fs.bytesRead = status.getFilesize();
        fs.file = file;
        fs.channel = channel;
        fs.channel.position(fs.bytesRead);
        fs.md = md5;
        fs.filename = status.getFilename();
        statusMap.put(status.getFilename(), fs);
        log.info("accept DestFileStatus of " + fs.file);
    }

    synchronized public void start() {
        exService = Executors.newSingleThreadExecutor();
        if (thread==null) {
            stop=false;
            thread = new Thread(this);
            thread.start();
        }
    }

    synchronized public void stop() {
        if (exService!=null)
            exService.shutdown();
        stop = true;
        if (thread!=null) {
            try {
                thread.join(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (thread.isAlive())
                thread.interrupt();
            thread = null;
        }
        else {
            cleanup();
        }
    }

}
